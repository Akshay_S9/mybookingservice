from django.urls import path
from . import views
from mybookingapp.views import CenterView, MeetingRoomView, DaypassView


urlpatterns = [
	#path('',views.index, name='index'),
	path('center/', CenterView.as_view()),
	path('center/<int:pk>', CenterView.as_view()),
	path('room/', MeetingRoomView.as_view()),
	path('room/<int:pk>', MeetingRoomView.as_view()),
	path('daypass/', DaypassView.as_view()),
	path('daypass/<int:pk>', DaypassView.as_view()),
]