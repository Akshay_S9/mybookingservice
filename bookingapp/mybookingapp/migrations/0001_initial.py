# Generated by Django 2.2.2 on 2019-06-25 18:29

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Center',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('center_name', models.CharField(max_length=200)),
                ('center_capacity', models.IntegerField(default=0)),
                ('center_address', models.CharField(max_length=1000)),
                ('center_daypass_price', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='MeetingRoom',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('room_name', models.CharField(max_length=200)),
                ('room_id', models.IntegerField(default=0)),
                ('room_capacity', models.IntegerField(default=0)),
                ('room_price', models.IntegerField(default=0)),
                ('center', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mybookingapp.Center')),
            ],
        ),
        migrations.CreateModel(
            name='DayPass',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('daypass_credits', models.IntegerField(default=0)),
                ('center', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='mybookingapp.Center')),
            ],
        ),
    ]
