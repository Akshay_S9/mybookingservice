from django.shortcuts import render
from django.http import HttpResponse
from rest_framework.views import APIView 
from rest_framework.response import Response
from .models import Center, MeetingRoom, DayPass
from .serializers import CenterSerializer, MeetingRoomSerializer, DayPassSerializer
from django.shortcuts import get_object_or_404
# Create your views here.

def index(request):
	return HttpResponse("Hi")

class CenterView(APIView):
	def get(self, request, pk):
		try:
			centers = Center.objects.get(pk=pk).all()
			serializer_center = CenterSerializer(centers, many="true")
			meetingrooms = MeetingRoom.objects.filter(center=pk)
			serializer_meetingroom = MeetingRoomSerializer(meetingrooms, many="true")
			daypass = DayPass.objects.filter(center=pk)
			serializer_daypass = DayPassSerializer(daypass, many="true")
			return Response({"centers": serializer_center.data, "meetingrooms": serializer_meetingroom.data, "daypass":serializer_daypass.data})
		except Center.DoesNotExist:
			return Response({'No Center data found for id: {}'.format(pk)})

	def post(self, request):
		center = request.data.get('center')
		serializer = CenterSerializer(data=center)
		if serializer.is_valid(raise_exception=True):
			center_saved = serializer.save()
		return Response({"success": "Center '{}' created successfully".format(center_saved.center_name)})

	def put(self, request, pk):
		try:
			saved_center = Center.objects.get(pk=pk)
			data = request.data.get('center')
			serializer = CenterSerializer(instance=saved_center, data=data, partial=True)
			if serializer.is_valid(raise_exception=True):
				center_saved = serializer.save()
			return Response({"success": "Center '{}' updated successfully".format(center_saved.center_name)})	
		except:
			return Response({'No Center data found for id: {}'.format(pk)})
	def delete(self, request, pk):
		'''center = get_object_or_404(Center.objects.all(), pk=pk)
		center.delete()'''
		try:
			center = Center.objects.get(pk=pk)
			center.delete()
			return Response({"message": "Center with id `{}` has been deleted.".format(pk)},status=204)
		except Center.DoesNotExist:
			return Response({'No Center data found for id: {}'.format(pk)})
		


class MeetingRoomView(APIView):
	def get(self, request, pk):
		try:
			meetingrooms = MeetingRoom.objects.get(pk=pk)
			serializer = MeetingRoomSerializer(meetingrooms, many="true")
			return Response({"meetingrooms": serializer.data})
		except MeetingRoom.DoesNotExist:
			return Response({'No Meeting Room found for id: {}'.format(pk)})
	def post(self, request):
		meetingroom = request.data.get('meetingroom')
		serializer = MeetingRoomSerializer(data=meetingroom)
		if serializer.is_valid(raise_exception=True):
			meetingroom_saved = serializer.save()
		return Response({"success": "Meeting Room '{}' created successfully".format(meetingroom_saved.room_name)})

	def put(self, request, pk):
		try:
			saved_room = MeetingRoom.objects.get(pk=pk)
			data = request.data.get('meetingroom')
			serializer = MeetingRoomSerializer(instance=saved_room, data=data, partial=True)
			if serializer.is_valid(raise_exception=True):
				room_saved = serializer.save()
			return Response({"Success": "Center '{}' updated successfully".format(room_saved.room_name)})
		except MeetingRoom.DoesNotExist:
			return Response({'No Meeting Room found for id: {}'.format(pk)})
	def delete(self, request, pk):
		'''meetingroom = get_object_or_404(MeetingRoom.objects.all(), pk=pk)
		meetingroom.delete()'''
		try:
			meetingroom = MeetingRoom.objects.get(pk=pk)
			meetingroom.delete()
			return Response({"message":"Center with id '{}' has been deleted.".format(pk)}, status=204)
		except MeetingRoom.DoesNotExist:
			return Response({'No Meeting Room found for id: {}'.format(pk)})

class DaypassView(APIView):
	def get(self, request, pk):
		try:
			daypass = DayPass.objects.get(pk=pk)
			serializer = DayPassSerializer(daypass, many="true")
			return Response({"daypass":serializer.data})
		except DayPass.DoesNotExist:
			return Response({'No DayPass found for id: {}'.format(pk)})
	def post(self, request):
		daypass = request.data.get('daypass')
		serializer = DayPassSerializer(data=daypass)
		if serializer.is_valid(raise_exception=True):
			daypass_saved = serializer.save()
		'''related_center = Center.objects.get(id=daypass["center"])
		related_center.center_capacity-=1
		print("-----")
		print(related_center.center_capacity)
		saved_center = CenterSerializer(data=related_center.__dict__)
		print("=====")
		print(related_center.__dict__)
		print(saved_center)
		if saved_center.is_valid(raise_exception=True):
			print("Into valid")
			center_saved = saved_center.save()
		print(center_saved.__dict__)
		print(related_center.center_capacity, related_center.center_name)'''
		return Response({"success":"Daypass '{}' created successfully".format(daypass_saved)})

	def put(self, request, pk):
		#saved_daypass = get_object_or_404(DayPass.objects.all(), pk=pk)
		try:
			saved_daypass = DayPass.objects.get(pk=pk)
			data = request.data.get('daypass')
			serializer = DayPassSerializer(instance=saved_daypass, data=data, partial=True)
			if serializer.is_valid(raise_exception=True):
				daypass_saved = serializer.save()
			return Response({"success":"DayPass '{}' updated successfully".format(daypass_saved)})
		except DayPass.DoesNotExist:
			return Response({'No DayPass found for id: {}'.format(pk)})

	def delete(self, request, pk):
		try:
			daypass = DayPass.objects.get(pk=pk)
			daypass.delete()
			return Response({"success":"Daypass with id '{}' has been deleted".format(pk)}, status=204)
		except DayPass.DoesNotExist:
			return Response({'No DayPass found for id: {}'.format(pk)})









