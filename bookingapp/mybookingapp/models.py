from django.db import models

# Create your models here.
class Center(models.Model):
	center_name = models.CharField(max_length=200)
	center_capacity = models.IntegerField(default=0)
	center_address = models.CharField(max_length=1000)
	center_daypass_price = models.IntegerField(default=0)

	def __str__(self):
		return self.center_name

class MeetingRoom(models.Model):
	room_name = models.CharField(max_length=200)
	room_id = models.IntegerField(default=0)
	room_capacity = models.IntegerField(default=0)
	room_price = models.IntegerField(default=0)
	center = models.ForeignKey(Center, on_delete=models.CASCADE, default=1, related_name='meeting_rooms') #define foreign key as fk_center_id

	def __str__(self):
		return self.room_name

class DayPass(models.Model):
	daypass_credits = models.IntegerField(default=0)
	center = models.ForeignKey(Center, on_delete=models.CASCADE)