from django.contrib import admin 
from .models import Center, MeetingRoom, DayPass
admin.site.register(Center)
admin.site.register(MeetingRoom)
admin.site.register(DayPass)