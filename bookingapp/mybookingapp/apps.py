from django.apps import AppConfig


class MybookingappConfig(AppConfig):
    name = 'mybookingapp'
