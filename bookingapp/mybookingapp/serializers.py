from rest_framework import serializers
from .models import Center, MeetingRoom, DayPass

'''class CenterSerializer(serializers.Serializer):
    center_name = serializers.CharField(max_length=200)
    center_capacity = serializers.IntegerField(default=0)
    center_address = serializers.CharField(max_length=1000)
    center_daypass_price = serializers.IntegerField(default=0)

    def create(self, validated_data):
        return Center.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.center_name = validated_data.get('center_name', instance.center_name)
        instance.center_capacity = validated_data.get('center_capacity', instance.center_capacity)
        instance.center_address = validated_data.get('center_address', instance.center_address)
        instance.center_daypass_price = validated_data.get('center_daypass_price',instance.center_daypass_price)
        instance.save()
        return instance

class MeetingRoomSerializer(serializers.Serializer):
    room_name = serializers.CharField(max_length=200)
    room_id = serializers.IntegerField(default=0)
    room_capacity = serializers.IntegerField(default=0)
    room_price = serializers.IntegerField(default=0)
    
    def create(self, validated_data):
        print("--------------")
        print(validated_data)

        return MeetingRoom.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.room_name = validated_data.get('room_name', instance.room_name)
        instance.room_id = validated_data.get('room_id', instance.room_id)
        instance.room_capacity = validated_data.get('room_capacity', instance.room_capacity)
        instance.room_price = validated_data.get('room_price', instance.room_price)
        instance.save()
        return instance
'''
class CenterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Center;
        fields = ('center_name','center_address','center_capacity', 'center_daypass_price', 'id')

class MeetingRoomSerializer(serializers.ModelSerializer):
    def create(self, validated_data):
        return MeetingRoom.objects.create(**validated_data) 
    class Meta:
        model = MeetingRoom;
        fields = ('room_price','room_name','room_id','room_capacity',"center")

class DayPassSerializer(serializers.ModelSerializer):
    class Meta:
        model = DayPass;
        fields = ("__all__")


